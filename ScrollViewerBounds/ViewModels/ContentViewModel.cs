﻿using GalaSoft.MvvmLight;

namespace ScrollViewerBounds.ViewModels
{
    public class ContentViewModel : ViewModelBase
    {
        public string Title { get; set; }
    }
}