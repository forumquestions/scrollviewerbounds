﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace ScrollViewerBounds.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public ObservableCollection<ContentViewModel> Content { get; }

        public MainViewModel()
        {
            Content = new ObservableCollection<ContentViewModel>();

            for (int i = 1; i <= 15; i++)
            {
                Content.Add(new ContentViewModel { Title = "Элемент " + i });
            }
        }
    }
}
