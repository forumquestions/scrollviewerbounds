﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace ScrollViewerBounds.Controls
{
    public sealed class ContentElement : Control
    {
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
            nameof(Title), typeof(string), typeof(ContentElement), new PropertyMetadata(default(string)));

        public ContentElement()
        {
            DefaultStyleKey = typeof(ContentElement);
        }
    }
}
