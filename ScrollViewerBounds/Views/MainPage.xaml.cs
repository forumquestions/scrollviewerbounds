﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using ScrollViewerBounds.ViewModels;

namespace ScrollViewerBounds.Views
{
    public sealed partial class MainPage : Page
    {
        public MainViewModel ViewModel
        {
            get { return (MainViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            nameof(ViewModel), typeof(MainViewModel), typeof(MainPage), new PropertyMetadata(default(MainViewModel)));

        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel = new MainViewModel();

            base.OnNavigatedTo(e);
        }
    }
}
